#!/bin/sh

files=`find . -name stats.txt`

echo Core-Type Cores L2-Size Time Energy ED
for file in $files
do
	type=`echo $file | awk -F'/' '{print $2}' | awk -F'_' '{print $2}'`
	cores=`echo $file | awk -F'/' '{print $2}' | awk -F'_' '{print $3}'`
	l2size=`echo $file | awk -F'/' '{print $2}' | awk -F'_' '{print $10}'`
	time=`grep 'Time' $file | awk '{print $4}'`
	energy=`grep 'Total energy' $file | awk '{print $5}'`
	ed=`grep 'Energy-Delay' $file | awk '{print $4}'`
	echo $type $cores $l2size $time $energy $ed
done
