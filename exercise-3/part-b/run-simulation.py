#!/usr/bin/python

import os

configs=open("l2_size_vs_cores.csv").readlines()

for config in configs:
	config_list = config.rstrip('\n').split(' ')

	os.system("zsim.sh -B -a blackscholes -b "+config_list[0]+" -c "+config_list[1]+" --l2size "+config_list[2])
