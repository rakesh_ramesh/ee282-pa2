Cores                  : 10
Frequency (MHz)        : 1700
Voltage (V)            : 1.0341
Area (mm^2)            : 325

Time (s)               : .64805882352941176470
Total energy (J)       : 41.52940623587586621614
Energy-Delay Product   : 26.91349814709673047641

Core dynamic energy (J): 3.09942128743133097540
Core static energy (J) : 3.04620150449117647050
L1 dynamic energy (J)  : .08120826842656230000
L1 static energy (J)   : .24639909335294117646
L2 dynamic energy (J)  : .00531969876815000000
L2 static energy (J)   : 28.71030199999999999973
L3 dynamic energy (J)  : .00599782281747000000
L3 static energy (J)   : 5.74274734235294117641
Mem dynamic energy (J) : .55899800000000000000
Mem static energy (J)  : .03281121823529411764
