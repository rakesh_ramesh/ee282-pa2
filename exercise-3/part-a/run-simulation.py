#!/usr/bin/python

import os

l2_sizes = [2**n for n in range(8, 16)]

# Wide
for core in range(1,9):
	max_l2_size = 0
	for l2_size in l2_sizes:
		os.system("zsim.sh -S -a blackscholes -b wide -c "+str(core)+" --l2size "+str(l2_size)+" | grep -c 'Validation error' > temp")
		line = (open("temp").readlines())[0].rstrip('\n')

		if int(line) > 0:
			break

		max_l2_size = l2_size
	
	print "wide", core, max_l2_size

# Narrow
for core in range(1, 17):
	max_l2_size = 0
	for l2_size in l2_sizes:
		os.system("zsim.sh -S -a blackscholes -b narrow -c "+str(core)+" --l2size "+str(l2_size)+" | grep -c 'Validation error' > temp")
		line = (open("temp").readlines())[0].rstrip('\n')

		if int(line) > 0:
			break

		max_l2_size = l2_size

	print "narrow", core, max_l2_size
