#!/usr/bin/python

import os

apps=['blackscholes', 'fluidanimate', 'streamcluster', 'swaptions', 'art']
cores=range(1,9)

for app in apps:
	for core in cores:
		os.system('zsim.sh -a '+app+' -B -c '+str(core))
