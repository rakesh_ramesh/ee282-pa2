Cores                  : 3
Frequency (MHz)        : 1700
Voltage (V)            : 1.07823
Area (mm^2)            : 149

Time (s)               : .61941176470588235294
Total energy (J)       : 16.65136915612331695231
Energy-Delay Product   : 10.31405395376344279455

Core dynamic energy (J): 7.08138433401732321708
Core static energy (J) : 2.63203476053894999997
L1 dynamic energy (J)  : .02976837046422750000
L1 static energy (J)   : .07065214994117647058
L2 dynamic energy (J)  : .27890904222721800000
L2 static energy (J)   : 1.03600519941176470588
L3 dynamic energy (J)  : .00167410540501000000
L3 static energy (J)   : 5.48889257647058823528
Mem dynamic energy (J) : .00068780000000000000
Mem static energy (J)  : .03136081764705882352
