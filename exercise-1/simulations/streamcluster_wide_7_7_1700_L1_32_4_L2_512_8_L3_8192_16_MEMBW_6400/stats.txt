Cores                  : 7
Frequency (MHz)        : 1700
Voltage (V)            : 1.07823
Area (mm^2)            : 307

Time (s)               : 1.61964705882352941176
Total energy (J)       : 52.84059055384616140669
Energy-Delay Product   : 85.58310707703530636279

Core dynamic energy (J): 7.08074848412677942449
Core static energy (J) : 16.05866146491788999993
L1 dynamic energy (J)  : .02683834355781810000
L1 static energy (J)   : .43106533951764705882
L2 dynamic energy (J)  : .30459295982863800000
L2 static energy (J)   : 6.32091073517647058821
L3 dynamic energy (J)  : .07797967025033000000
L3 static energy (J)   : 14.35243762588235294113
Mem dynamic energy (J) : 8.10535320000000000000
Mem static energy (J)  : .08200273058823529411
