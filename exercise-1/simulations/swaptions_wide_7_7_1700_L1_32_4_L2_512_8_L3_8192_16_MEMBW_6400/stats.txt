Cores                  : 7
Frequency (MHz)        : 1700
Voltage (V)            : 1.07823
Area (mm^2)            : 307

Time (s)               : .26835294117647058823
Total energy (J)       : 13.53484125670967265279
Energy-Delay Product   : 3.63211445959467803769

Core dynamic energy (J): 7.08215655175911403530
Core static energy (J) : 2.66069636097026999993
L1 dynamic energy (J)  : .02979642158860050000
L1 static energy (J)   : .07142151808235294117
L2 dynamic energy (J)  : .24708363934667400000
L2 static energy (J)   : 1.04728680082352941174
L3 dynamic energy (J)  : .00343380060972000000
L3 static energy (J)   : 2.37799885411764705877
Mem dynamic energy (J) : .00138060000000000000
Mem static energy (J)  : .01358670941176470588
