#!/bin/sh

files=`find ../simulations -name stats.txt`

echo App Cores Time Energy ED
for file in $files
do
	app=`echo $file | awk -F'_' '{print $1}' | awk -F'/' '{print $3}'`
	core=`echo $file | awk -F'_' '{print $3}'`
	time=`grep 'Time' $file | awk '{print $4}'`
	energy=`grep 'Total energy' $file | awk '{print $5}'`
	ed=`grep 'Energy-Delay' $file | awk '{print $4}'`
	echo $app $core $time $energy $ed
done
