#!/usr/bin/python

import os

membw_sweep = range(1000, 10000, 400)

for membw in membw_sweep:
	os.system('zsim.sh -a art -c 8 -B --membw '+str(membw))
