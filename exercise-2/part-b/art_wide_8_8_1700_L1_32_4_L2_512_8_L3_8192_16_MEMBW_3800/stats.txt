Cores                  : 8
Frequency (MHz)        : 1700
Voltage (V)            : 1.07823
Area (mm^2)            : 347

Time (s)               : 1.48594117647058823529
Total energy (J)       : 62.23594188682713525627
Energy-Delay Product   : 92.47894870606707433554

Core dynamic energy (J): 7.08081037398817460348
Core static energy (J) : 16.83769043012963999992
L1 dynamic energy (J)  : .03496478853845430000
L1 static energy (J)   : .45197694451764705882
L2 dynamic energy (J)  : .20116195728080400000
L2 static energy (J)   : 6.62754728517647058821
L3 dynamic energy (J)  : .49981180778418000000
L3 static energy (J)   : 13.16760829764705882349
Mem dynamic energy (J) : 17.25913680000000000000
Mem static energy (J)  : .07523320176470588235
