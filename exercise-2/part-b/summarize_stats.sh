#!/bin/sh

files=`find . -name stats.txt`

echo Membw Time Energy ED
for file in $files
do
	membw=`echo $file | awk -F'/' '{print $2}' | awk -F'_' '{print $16}' | sed 's:/::'`
	time=`grep 'Time' $file | awk '{print $4}'`
	energy=`grep 'Total energy' $file | awk '{print $5}'`
	ed=`grep 'Energy-Delay' $file | awk '{print $4}'`
	echo $membw $time $energy $ed
done
